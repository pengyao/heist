=====
Heist
=====

Heist creates network tunnels for distributing and managing agents. While it has
been originally built to deploy and manage Salt minions, it can be used to
distribute and manage other agents or plugins if extended to do so.

Using Heist is very easy. Start by downloading Heist. Just install via ``pip``:

.. code-block:: bash

    pip install heist


Heist Manager
=============

In order to run Heist you will need to specify a Heist manager. The default
Heist manager included with Heist is called ``test``. When you run Heist with
the ``test`` manager it will just log a statement stating Heist is installed
correctly and to use a fully functioning Heist manager. This is used just for
testing your setup in Heist.

Another example of a Heist manager is ``salt.minion`` which is used to managed a
Salt minion. It is made available by installing
`heist-salt <https://heist-salt.readthedocs.io/en/latest/>`_ using ``pip``:

.. code-block:: bash

    pip install heist-salt


Making Your Roster
==================

A Roster is a file used by Heist to map login information to the systems in your
environment. This file can be very simple and just needs to tell Heist where
your systems are and how to log into them via SSH. Open a file called
`roster.cfg` and add the data needed to connect to a remote system via SSH:

.. code-block:: yaml

    system_name:
      host: 192.168.4.4
      username: fred
      password: freds_password

.. note::

    This example is very simple, Heist supports virtually all available authentication
    options for SSH.

All roster files typically live inside of a roster directory. But to get started
we will execute a single roster file with Heist using the ``heist-salt``
manager:

.. code-block:: bash

    heist salt.minion -R roster.cfg

Assuming your roster is correct, Heist will now connect to the remote
system, and deploy the salt minion binary.


Tear Down
=========

Heist is able to automatically clean up as well! Just soft kill your Heist
application and it will reach out to all connections, tell them to remove the
deployed artifacts from the target systems and stop the service! Like a proper
Heist there should be no evidence left behind!


Using Heist to Deploy Salt Minions
==================================
If you want to use Heist to deploy and manage Salt, you will need to install
`heist-salt <https://heist-salt.readthedocs.io/en/latest/>`_.


Using the Test manager in Heist
===============================
You can use the ``test`` manager in Heist to ensure your Heist install is setup
correctly. You just need to run:

.. code-block:: bash

    heist test -R roster.cfg

If successful, the test manager will log that you successfully used the ``test``
manager and inform you that you need to use a different manager for full
functionality. This test manager will ensure functionality of Heist up until
it calls a manager. It will detect roster rendering failures and dependency
install issues.
