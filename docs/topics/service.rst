==============
Service Plugin
==============

Heist can use a system manager to manage the service of the artifact. By default
it will not use a service manager and start the service in the background. If
you want to use a service manager you have a couple of options:

1. Set ``service_plugin`` to the service manager you want to use on the target
   host in the Heist config.

    .. code-block:: yaml

        heist:
          service_plugin: systemd

2. Set ``auto_service`` to ``True`` in Heist's config and Heist will auto detect
   the system manager.

    .. code-block:: yaml

        heist:
          auto_service: True

3. Set ``service_plugin`` in your roster file to the service manager you want to
   use on the target host.

    .. code-block:: yaml

        system_name:
          host: 10.0.0.28
          username: root
          password: "password"
          service_plugin: systemd

.. note::

    The only service managers currently supported are:

    - systemd
    - win_service
