#!/usr/bin/python3
"""
    tests.unit.artifact.test_init
    ~~~~~~~~~~~~~~

    tests for artifact.init code
"""
import pathlib
import re
import sys
import tarfile
import tempfile
import unittest.mock as mock
import zipfile
from unittest.mock import call
from unittest.mock import MagicMock
from unittest.mock import Mock
from unittest.mock import patch

import aiohttp
import pytest
from dict_tools.data import NamespaceDict

import tests.helpers
from tests.helpers import TEST_FILES


@pytest.mark.asyncio
async def test_clean(mock_hub, hub):
    """
    test artifact.init.clean removes the correct directory
    """
    mock_hub.artifact.init.clean = hub.artifact.init.clean
    target_name = "test_target_name"
    run_dir = pathlib.Path("var") / "tmp" / "heist_root" / "abcd"
    scripts_dir = run_dir / "scripts"
    alias = "test_binary"
    mock_hub.heist.CONS = {}
    mock_hub.heist.CONS[target_name] = {}
    mock_hub.heist.CONS[target_name]["run_dir"] = run_dir

    mock_hub.tool.system.os_arch.return_value = ("linux", "amd64")
    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(
        returncode=0, stdout=alias
    )
    await mock_hub.artifact.init.clean(target_name, "asyncssh")

    assert mock_hub.tunnel.asyncssh.cmd.call_args_list[0] == call(
        target_name, f"ls {scripts_dir}", target_os="linux"
    )
    assert mock_hub.tunnel.asyncssh.cmd.call_args_list[1] == call(
        target_name,
        f"find -L /usr/bin/ -samefile {scripts_dir / alias}",
        target_os="linux",
    )
    assert mock_hub.tunnel.asyncssh.cmd.call_args_list[2] == call(
        target_name, f"rm {alias}", target_os="linux"
    )
    assert mock_hub.tunnel.asyncssh.cmd.call_args_list
    assert mock_hub.tunnel.asyncssh.cmd.call_args_list[3] == call(
        target_name, f"[ -d {run_dir} ]", target_os="linux"
    )

    assert mock_hub.tunnel.asyncssh.cmd.call_args_list[4] == call(
        target_name, f"rm -rf {run_dir}", target_os="linux"
    )
    assert mock_hub.tunnel.asyncssh.cmd.call_args_list[5] == call(
        target_name, f"rmdir {run_dir.parent}", target_os="linux"
    )


@pytest.mark.parametrize(
    "hash_value,expected",
    [
        (
            "a69f73cca23a9ac5c8b567dc185a756e97c982164fe25859e0d1dcc1475c80a615b2123af1f5f94c11e3e9402c3ac558f500199d95b6d3e301758586281dcd26",
            True,
        ),
        ("1234", False),
    ],
)
def test_verify(hub, mock_hub, hash_value, expected):
    """
    test verify when hash is correct
    """
    mock_hub.artifact.init.verify = hub.artifact.init.verify
    if expected:
        assert mock_hub.artifact.init.verify(
            location=TEST_FILES / "sha_file_test",
            hash_value=hash_value,
            hash_type="sha3_512",
        )
    else:
        assert not mock_hub.artifact.init.verify(
            location=TEST_FILES / "sha_file_test",
            hash_value=hash_value,
            hash_type="sha3_512",
        )


@pytest.mark.asyncio
async def test_fetch(hub, mock_hub, httpserver):
    """
    test artifacts.init.fetch when 200 returned
    and download or location are not set
    """
    mock_hub.artifact.init.fetch = hub.artifact.init.fetch
    exp_ret = {"test": "true"}
    httpserver.expect_request("/test").respond_with_json(exp_ret)
    url = httpserver.url_for("/test")
    async with aiohttp.ClientSession() as session:
        ret = await mock_hub.artifact.init.fetch(session=session, url=url)
    assert ret == exp_ret


@pytest.mark.parametrize(
    "target_os,zip_file",
    [
        (
            "linux",
            "linux-3003.tar.gz",
        ),
        ("windows", "windows-3003.zip"),
    ],
)
@pytest.mark.asyncio
async def test_extract(hub, mock_hub, target_os, zip_file):
    """
    test artifact.init.extract
    """
    mock_hub.artifact.init.extract = hub.artifact.init.extract
    target_name = "test_target_name"
    run_dir = pathlib.Path("run_dir")
    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(returncode=0)

    ret = await mock_hub.artifact.init.extract(
        target_name, "asyncssh", binary=zip_file, run_dir=run_dir, target_os=target_os
    )

    if target_os == "linux":
        assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
            target_name,
            f"tar -xvf {run_dir / zip_file} --directory={run_dir}",
            target_os="linux",
        )
    else:
        assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
            target_name,
            f'powershell -command "$ProgressPreference = "SilentlyContinue"; Expand-Archive -LiteralPath "{run_dir / zip_file}" -DestinationPath {run_dir}"',
            target_os="windows",
        )
    assert ret


@pytest.mark.parametrize(
    "version",
    [
        ("1.2.3.4-1"),
        ("1.2.3.4"),
    ],
)
@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.asyncio
async def test_get(hub, mock_hub, tmp_path, version):
    """
    test artifacts.init.get
    """
    tests.helpers.mock_artifacts(mock_hub)
    mock_hub.artifact.init.get = hub.artifact.init.get
    mock_temp_dir = tempfile.TemporaryDirectory()
    mock_hub.OPT.heist = NamespaceDict(artifacts_dir=str(tmp_path))
    files_in_archive = ["test1", "test2"]
    with tempfile.TemporaryDirectory() as tmpdirname:
        tar_file = pathlib.Path(tmpdirname, f"test_artifact-{version}.tar.gz")
        create_tar = tarfile.open(tar_file, "w:gz")
        for file_name in files_in_archive:
            full_path = pathlib.Path(tmpdirname) / file_name
            with open(full_path, "w") as fp:
                fp.write("")
            create_tar.add(full_path, arcname=file_name)
        create_tar.close()
        mock_hub.artifact.test.get.return_value = tar_file
        with patch("tempfile.TemporaryDirectory", Mock(return_value=mock_temp_dir)):
            ret = await mock_hub.artifact.init.get(
                artifact_name="test", target_os="linux", version=version, repo_data={}
            )
        exp_artifact_file = tmp_path / tar_file.name
        assert ret == exp_artifact_file
        assert exp_artifact_file.exists()


@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.asyncio
async def test_get_bad_path(hub, mock_hub, tmp_path):
    """
    test artifacts.init.get when tmp_artifact_location includes "../"
    """
    tests.helpers.mock_artifacts(mock_hub)
    mock_hub.artifact.init.get = hub.artifact.init.get
    mock_hub.tool.path.clean_path = hub.tool.path.clean_path
    mock_temp_dir = tempfile.TemporaryDirectory()
    mock_hub.OPT.heist = NamespaceDict(artifacts_dir=tmp_path)
    mock_hub.artifact.test.get.return_value = pathlib.Path("../")
    with patch("tempfile.TemporaryDirectory", Mock(return_value=mock_temp_dir)):
        ret = await mock_hub.artifact.init.get(
            artifact_name="test", target_os="linux", version="1.2.3.4", repo_data={}
        )
    assert not ret


@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.asyncio
async def test_get_tmp_art_return_false(hub, mock_hub, tmp_path):
    """
    test artifacts.init.get when tmp_artifact_location returns False
    """
    tests.helpers.mock_artifacts(mock_hub)
    mock_hub.artifact.init.get = hub.artifact.init.get
    mock_temp_dir = tempfile.TemporaryDirectory()
    mock_hub.OPT.heist = NamespaceDict(artifacts_dir=tmp_path)
    with patch("tempfile.TemporaryDirectory", Mock(return_value=mock_temp_dir)):
        mock_hub.artifact.test.get.return_value = False
        ret = await mock_hub.artifact.init.get(
            artifact_name="test", target_os="linux", version="1.2.3.4", repo_data={}
        )
    assert not ret


@pytest.mark.skipif(sys.version_info < (3, 8), reason="AsyncMock was introduced in 3.8")
@pytest.mark.parametrize(
    "returncode,verify",
    [
        (
            0,
            True,
        ),
        (1, False),
    ],
)
@pytest.mark.parametrize(
    "target_os",
    ["windows", "linux"],
)
@pytest.mark.asyncio
async def test_deploy_verify(hub, mock_hub, tmp_path, target_os, returncode, verify):
    """
    test artifacts.init.deploy when artifact needs to be
    deployed and when its already been deployed and we
    need to verify the artifact
    """
    tests.helpers.mock_artifacts(mock_hub)
    target_name = "test_name"
    run_dir = tmp_path / "run_dir"
    binary = tmp_path / "test-binary.zip"
    tunnel_plugin = "asyncssh"
    prev_pkg_name = None
    if verify:
        prev_pkg_name = "pkg-3000-1.tar.gz"
    mock_hub.artifact.init.deploy = hub.artifact.init.deploy
    mock_hub.tool.artifacts.get_artifact_suffix.return_value = "tar.gz"
    mock_hub.tunnel.asyncssh.cmd.side_effect = [
        NamespaceDict(returncode=returncode, stdout=f"{run_dir}\n"),
        NamespaceDict(returncode=returncode, stdout=f"test test2 {prev_pkg_name}"),
    ]
    mock_hub.artifact.test.deploy.return_value = binary
    ret = await mock_hub.artifact.init.deploy(
        target_name=target_name,
        tunnel_plugin=tunnel_plugin,
        run_dir=run_dir,
        binary=binary,
        artifact_name="test",
        target_os=target_os,
        run_dir_root=run_dir.parent,
    )
    exp_cmd = f"/bin/sh << 'EOF'\nif [ -d {run_dir.parent} ]; then\nls {run_dir.parent};\nexit 0\nelse\necho \"The {run_dir.parent} does not exist. Deploying artifact\"\nexit 1\nfi\nEOF"
    if target_os == "windows":
        exp_cmd = (
            "powershell -command "
            + '"'
            + "; ".join(
                [
                    f"$Folder = '{run_dir.parent}'",
                    "if (Test-Path -Path $Folder) {Get-ChildItem -name $Folder} else {exit 1}",
                ]
            )
            + '"'
        )
    assert mock_hub.tunnel.asyncssh.cmd.call_args_list[0] == call(
        target_name, exp_cmd, target_os=target_os
    )
    assert ret == (run_dir, binary, verify, prev_pkg_name)

    kwargs = {"target_os": target_os}
    if verify:
        kwargs["verify"] = verify
    assert mock_hub.artifact.test.deploy.call_args_list[0] == call(
        target_name,
        tunnel_plugin,
        run_dir,
        binary,
        run_dir_root=run_dir.parent,
        **kwargs,
    )


@pytest.mark.parametrize(
    "target_os",
    ["windows", "linux"],
)
@pytest.mark.asyncio
async def test_verify_checksum(hub, mock_hub, tmp_path, target_os):
    """"""
    #    tests.helpers.mock_artifacts(mock_hub)
    target_name = "test_name"
    tunnel_plugin = "asyncssh"
    run_dir = tmp_path / "run_dir"
    artifacts_dir = tmp_path / "artifacts"
    artifacts_dir.mkdir()
    source_fp = artifacts_dir / "code_checksum"
    target_fp = run_dir / "code_checksum"
    mock_hub.artifact.init.verify_checksum = hub.artifact.init.verify_checksum
    target_ret = (
        "ac17b74c18f01dcdcace77c9ae669a1af560281bfee40930f1e7267a90d128a0fd96d63f44868e9355559314238dd21d0d3465011883525d39da62ad5bf4c1d2 root_dir/conf/minion"
        "f6e50081afea69ce74686cb4e9dc289fbfa1c47907ccc6f619e2ad3330f652a0d3e7cb3a1f305fe1eaab2161c3cf1e85a9bc92b4698944439d32f72bfdd57cd3 salt-3005-1-linux-amd64.tar.gz"
        "dae837fef0610ef3ba81a87c121ded5ce35d9257401eae177bd4c84b4e74fbf2c141572702f88e929aa122011f9035962266fadfed88b1b0376b0f2d933df468 salt/run/cffi-1.14.6.dist-info/LICENSE"
    )
    if target_os == "windows":
        target_ret = (
            "\r\n\r\nAlgorithm : SHA512\r\nHash      :               "
            "DC28E13106A4C29AA2078386090479C1009934F0DF72D5BEB7797225AC02CA7146F7E929776356B6DAFE7297CF11E9EFEBB2F1737ED\r\n        "
            "0583053281B679A99A71B\r\nPath      : C:\\Windows\\temp\\heist_administrator\\a047\\code_checksum\r\n\r\nAlgorithm : SHA512\r\nHash      :   "
            "375B9642DCF92BD5A29B0F64A3941A091D660B2231D2C5FD01011F63F406FD73B29404886BD5EDB4134F03A41C67D671D775B4A0813\r\n "
            "14724828C47FB31AF9018\r\nPath      : C:\\Windows\\temp\\heist_administrator\\a047\\salt-3005-1-windows-amd64.zip\r\n\r\nAlgorithm :        "
            "SHA512\r\nHash      : 3F6AE2723D9D89A134D331625FD470401D71F6E3AAAB80262DD3A410302590DD71E53455CF54C25E45E87DD86209B1CDCE347ECA574\r\n  "
            "   BAD19ABCFBA78FB0CEEB3\r\nPath      : C:\\Windows\\temp\\heist_administrator\\a047\\root_dir\\conf\\minion\r\n\r\nAlgorithm :           "
            "SHA512\r\nHash      : 84AEA0A158EE18D3A05B60E79DA2DD2602A09240CF6F93B2A84D7DB1D7F7483C8D9F7CC4EB546B0DE954FCDFF556B9CC2186CE8352C\r\n "
            "   94F8FD08D4D4BD7128AD7\r\nPath      :     "
            "C:\\Windows\\temp\\heist_administrator\\a047\\root_dir\\conf\\pki\\minion\\minion.pem\r\n\r\nAlgorithm : SHA512\r\nHash      :         "
            "D51B83C653FFD51021632619BC848CB71A81234499859F9C2393FC4C945FB4B896F483DFB3CF40FBE22A942D1140A48E0DCC920C93C\r\n "
            "5E974B793A9048CAB42A0\r\n"
        )
        source_content = (
            "375b9642dcf92bd5a29b0f64a3941a091d660b2231d2c5fd01011f63f406fd73b29404886bd5edb4134f03a41c67d671d775b4a081314724828c47fb31af9018 salt-3005-1-windows-amd64.zip\n"
            "3f6ae2723d9d89a134d331625fd470401d71f6e3aaab80262dd3a410302590dd71e53455cf54c25e45e87dd86209b1cdce347eca574bad19abcfba78fb0ceeb3 root_dir/conf/minion"
        )
    with open(source_fp, "w") as fp:
        if target_os == "linux":
            fp.write(target_ret)
        else:
            fp.write(source_content)

    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(
        returncode=0, stdout=target_ret
    )

    patch_file = patch.object(pathlib.Path, "is_file")
    patch_file.return_value = True
    with patch_file:
        ret = await mock_hub.artifact.init.verify_checksum(
            target_name=target_name,
            tunnel_plugin=tunnel_plugin,
            run_dir=run_dir,
            source_fp=source_fp,
            target_fp=target_fp,
            target_os=target_os,
        )
    exp_cmd = f"cd {run_dir}; sha512sum -c {target_fp}"
    if target_os == "windows":
        exp_cmd = "; ".join(
            [
                f'powershell -command "Get-ChildItem -Recurse {target_fp.parent} | Get-FileHash -Algorithm SHA512 | Format-List"'
            ]
        )
    assert mock_hub.tunnel.asyncssh.cmd.call_args_list[0] == call(
        target_name, exp_cmd, target_os=target_os
    )
    assert ret == {"returncode": 0, "stdout": target_ret}


def test_checksum(hub, mock_hub, tmp_path):
    """"""
    mock_hub.artifact.init.checksum = hub.artifact.init.checksum
    sub_dir = tmp_path / "sub_dir"
    sub_dir.mkdir()
    files = [
        tmp_path / "test1",
        tmp_path / "test2",
        tmp_path / "test3",
        sub_dir / "sub1",
        sub_dir / "sub2",
        sub_dir / "sub3",
    ]

    for _file in files:
        with open(_file, "w") as fp:
            fp.write(_file.name)
    ret = mock_hub.artifact.init.checksum(files)
    check = r"([a-fA-F\d]){128}"
    assert not any([x for x in files if x not in ret.values()])
    assert not any([x for x in ret.keys() if not re.match(check, x)])


@pytest.mark.parametrize(
    "target_os",
    ["windows", "linux"],
)
@pytest.mark.asyncio
async def test_create_aliases(hub, mock_hub, tmp_path, target_os):
    """
    Test artifact.init.create_aliases
    """
    content = "echo test" "/var/tests {alias} -c /var/config/"
    aliases = {
        "test1": {"file": tmp_path / "test1", "cmd": "test1cmd"},
        "test2": {"file": tmp_path / "test2", "cmd": "test2cmd"},
    }
    artifacts_dir = tmp_path / "artifacts"
    artifacts_dir.mkdir()
    mock_hub.artifact.init.create_aliases = hub.artifact.init.create_aliases
    mock_hub.tool.artifacts.get_artifact_dir.return_value = artifacts_dir
    ret = await mock_hub.artifact.init.create_aliases(
        content, aliases=aliases, target_os=target_os
    )
    for alias in aliases:
        alias_file = aliases[alias]["file"]
        if target_os == "windows":
            alias_file = alias_file.parent / f"{alias_file.name}.bat"
        with open(alias_file) as fp:
            assert fp.read() == content.format(alias=aliases[alias]["cmd"])


@pytest.mark.parametrize(
    "target_os",
    ["windows", "linux"],
)
@pytest.mark.asyncio
async def test_deploy_aliases(hub, mock_hub, tmp_path, target_os):
    """
    Test artifact.init.deploy_aliases
    """
    artifacts_dir = tmp_path / "artifacts"
    artifacts_dir.mkdir()
    run_dir = tmp_path / "run_dir"
    run_dir.mkdir()
    scripts_dir = run_dir / "scripts"
    target_name = "test_name"
    tunnel_plugin = "asyncssh"
    mock_hub.artifact.init.deploy_aliases = hub.artifact.init.deploy_aliases
    mock_hub.tool.artifacts.get_artifact_dir.return_value = artifacts_dir
    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(returncode=0)
    assert await mock_hub.artifact.init.deploy_aliases(
        target_name, tunnel_plugin, run_dir, scripts_dir, target_os=target_os
    )
    exp_cmd_ret = f"ln -s {scripts_dir}/* /usr/bin/"
    if target_os == "windows":
        exp_cmd_ret = f'powershell -command "setx PATH "{scripts_dir};$env:PATH""'
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, exp_cmd_ret, target_os=target_os
    )
