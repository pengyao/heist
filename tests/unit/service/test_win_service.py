import sys
from pathlib import PurePosixPath
from unittest.mock import call

import pytest


@pytest.mark.asyncio
async def test_status_service(hub, mock_hub):
    """
    Test getting status of a windows service
    """
    target_name = "1234"
    service_name = "test-service"
    mock_hub.service.win_service.status = hub.service.win_service.status
    await mock_hub.service.win_service.status(
        target_name=target_name, tunnel_plugin="asyncssh", service=service_name
    )
    assert mock_hub.tunnel.asyncssh.cmd.call_args == call(
        target_name, f"Get-Service {service_name}"
    )
