#!/usr/bin/python3
import sys

import pytest
from dict_tools.data import NamespaceDict

import heist.roster.scan
import tests.helpers

if sys.version_info >= (3, 8):
    from unittest.mock import patch as async_patch
else:
    from asynctest.mock import patch as async_patch


@pytest.mark.asyncio
async def test_read(mock_hub):
    """
    test scan roster against port 22 localhost
    """
    # Setup
    target = "127.0.0.1"
    mock_hub.OPT = NamespaceDict(heist={"target": target, "ssh_scan_ports": [22]})

    # Execute
    with async_patch(
        "asyncio.open_connection", side_effect=tests.helpers.async_read_write
    ):

        ret = await heist.roster.scan.read(mock_hub)

    assert ret[target]["host"] == target
    assert ret[target]["port"] == 22


@pytest.mark.asyncio
async def test_scan_non_ssh_port(mock_hub):
    """
    test scan roster against port not running ssh
    """
    # Setup
    target = "127.0.0.1"
    mock_hub.OPT = NamespaceDict(heist={"target": target, "ssh_scan_ports": [22222]})

    # Execute
    ret = await heist.roster.scan.read(mock_hub)

    assert ret == {}
