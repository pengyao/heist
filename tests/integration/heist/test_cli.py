import sys
import tempfile

import pytest
import yaml


@pytest.mark.skipif(
    sys.version_info < (3, 8), reason="This fixture is only available on >= python 3.8"
)
def test_default_config(cli_runpy):
    """
    Verify the default config values
    """
    ret = cli_runpy("test", "--config-template")

    assert ret.json.heist.artifact_version == ""
    assert ret.json.heist.artifacts_dir == "/var/tmp/heist/artifacts"
    assert ret.json.heist.auto_service is False
    assert ret.json.heist.checkin_time == 60
    assert ret.json.heist.clean is False
    assert ret.json.heist.dynamic_upgrade is False
    assert ret.json.heist.manage_service is None
    assert ret.json.heist.renderer == "yaml"
    assert ret.json.heist.roster is None
    assert ret.json.heist.roster_data is None
    assert ret.json.heist.roster_defaults == {}
    assert ret.json.heist.roster_dir == "/etc/heist/rosters"
    assert ret.json.heist.roster_file
    assert ret.json.heist.run_dir_root is False
    assert ret.json.heist.service_plugin == "raw"
    assert ret.json.heist.target == ""
    # assert ret.json.heist.tunnel_plugin == "asyncssh"


@pytest.mark.skipif(
    sys.version_info < (3, 8), reason="This fixture is only available on >= python 3.8"
)
def test_help(cli_runpy):
    """
    Verify that the test heist manager is callable from the cli
    """
    ret = cli_runpy("test", "--help", parse_output=False)
    assert ret.stdout


@pytest.mark.skipif(
    sys.version_info < (3, 8), reason="This fixture is only available on >= python 3.8"
)
def test_basic(cli_runpy, roster, openssh_container):
    with tempfile.NamedTemporaryFile(delete=True, mode="w+") as fh:
        yaml.safe_dump(roster, fh)
        fh.flush()
        ret = cli_runpy("test", "-R", fh.name, parse_output=False)
    assert not bool(ret.stderr), ret.stderr
    # assert not single["retvalue"], single["comment"]
    assert "Success" in ret.stdout
    assert "No problems encountered" in ret.stdout
