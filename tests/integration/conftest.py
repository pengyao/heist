import sys
import tempfile
import unittest.mock as mock

import docker
import pytest


@pytest.fixture(name="roster", scope="function")
def get_roster() -> dict:
    """
    Return a temporary file that can be used as a roster
    """
    roster = {}
    yield roster


@pytest.fixture(scope="function")
def hub(hub, tmp_path):
    hub.pop.sub.add(dyne_name="heist")

    heist_conf = tmp_path / "heist.conf"
    with open(heist_conf, "w") as fp:
        pass

    with mock.patch("sys.argv", ["heist", "test", f"--config={heist_conf}"]):
        hub.pop.config.load(
            ["heist", "rend"],
            cli="heist",
            parse_cli=False,
        )

    yield hub


@pytest.fixture(scope="function")
def openssh_container(roster):
    client = docker.from_env()

    # Detect if running with Docker-in-Docker or local Docker
    networks = client.networks.list(names=["docker"])

    if networks:
        network_name = "docker"
        host = "docker"
    else:
        network_name = "bridge"
        host = "localhost"

    container = client.containers.run(
        # https://hub.docker.com/r/linuxserver/openssh-server
        "linuxserver/openssh-server:latest",
        command=["/bin/sh", "-c", "while true; do sleep 1; done"],
        detach=True,
        ports={"2222/tcp": 2222},
        hostname="heist_test",
        network=network_name,
        environment={
            "PUID": "1000",
            "PGID": "1000",
            "TZ": "Etc/UTC",
            "SUDO_ACCESS": "true",
            "PASSWORD_ACCESS": "true",
            "USER_NAME": "user",
            "USER_PASSWORD": "pass",
        },
    )
    try:
        container.reload()

        with tempfile.TemporaryDirectory(prefix="heist_", suffix="_tests") as td:
            roster["heist_test"] = {
                "host": host,
                "id": host,
                "port": 2222,
                "username": "user",
                "password": "pass",
                "known_hosts": None,
                "artifacts_dir": td,
            }
            yield roster["heist_test"]
    finally:
        container.stop()
        container.remove()
