﻿# This script supports Windows 8+ / Windows Server 2012+
function Write-Message {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        # The message to output
        [String] $Message,

        [Parameter(Mandatory=$false)]
        # The message to output
        [Int] $Length = 73
    )
    $msg = "- " + $Message + ":"
    $msg = $msg + " " * ($Length - $msg.Length)
    Write-Host $msg -NoNewLine
}

Write-Host "********************************************************************************"
Write-Host "Install OpenSSH Script"
Write-Host "********************************************************************************"
Write-Message "Enabling TLS1.2 support"
try {
    [System.Net.ServicePointManager]::SecurityProtocol = `
            [System.Net.SecurityProtocolType]'Tls12'
    Write-Host "Success" -ForegroundColor Green
} catch {
    Write-Host "Failed" -ForegroundColor Red
    Write-Host "*** Error message: $_" -ForegroundColor Red
    exit 1
}

$global:ErrorActionPreference = "Stop"
$global:ProgressPreference = "SilentlyContinue"

Write-Message "Ensuring script is run as Administrator"
$Identity = [System.Security.Principal.WindowsIdentity]::GetCurrent()
$Principal = New-Object System.Security.Principal.WindowsPrincipal($Identity)
$AdminRole = [System.Security.Principal.WindowsBuiltInRole]::Administrator
if (!($Principal.IsInRole($AdminRole))) {
    Write-Host "Failed" -ForegroundColor Red
    Write-Host "*** This script must run as Administrator" -ForegroundColor Red
    exit 1
}
Write-Host "Success" -ForegroundColor Green

# Version and download URL
$OPENSSH_VERSION = "V8.6.0.0p1-Beta"
$OPENSSH_URL = "https://github.com/PowerShell/Win32-OpenSSH/releases/download/$OPENSSH_VERSION/OpenSSH-Win64.zip"

# Set various known paths
$OPENSSH_ZIP = "$($env:TEMP)\OpenSSH.zip"
$DATA_DIR = "$($env:ProgramData)\ssh"
$INSTALL_DIR = "$($env:ProgramFiles)\OpenSSH"


function Get-WebFile {
    # Downloads a file from the web. If the download times out, wait 10 seconds
    # and try again. Continues retrying until the download retry count max is
    # reached. Default is 10 retries
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        # Url (string): The url for the file to download
        [String] $Url,

        [Parameter(Mandatory=$true)]
        # OutFile (string): The location to put the downloaded file
        [String] $OutFile,

        [Parameter(Mandatory=$false)]
        # Retries(int): The number of times to attempt downloading before
        # giving up. Default is 10
        [Int] $Retries = 10
    )
    # Make sure the target directory exists
    $parent_dir = Split-Path $OutFile
    if ( !( Test-Path $parent_dir )) {
        Write-Message "Creating missing download directory"
        try {
            New-Item $parent_dir -Type Directory | Out-Null
            Write-Host "Success" -ForegroundColor Green
        } catch {
            Write-Host "Failed" -ForegroundColor Red
            Write-Host "*** Error message: $_" -ForegroundColor Red
            exit 1
        }
    }

    $tries = 1
    $success = $false
    while (!$success){
        try {
            # Download the file
            Write-Message "Downloading (try: $tries/$Retries)"
            Invoke-WebRequest -Uri $Url -OutFile $OutFile
        } catch {
            Write-Host "Failed" -ForegroundColor Red
            Write-Host "*** Error downloading: $Url"
            Write-Host "*** Error message: $_" -ForegroundColor Red
        } finally {
            $tries++
            if ((Test-Path -Path "$OutFile") -and ((Get-Item "$OutFile").Length -gt 0kb
            )) {
                Write-Host "Success" -ForegroundColor Green
                $success = $true
            } else {
                Write-Host "Failed" -ForegroundColor Red
                if ($tries -gt $Retries) {
                    Write-Host "*** Retry count exceeded"
                    exit 1
                }
                Write-Host "*** Trying again after 10 seconds"
                Start-Sleep -Seconds 10
            }
        }
    }
}

function Expand-ZipFile {
    # Extract a zip file
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $true)]
        #The path to the file to extract
        [string] $ZipFile,

        [Parameter(Mandatory = $true)]
        # The location to extract to. Must be a directory
        [string] $Destination,

        [Parameter(Mandatory = $false)]
        # Remove the zipfile after extracting
        [Switch] $Clean = $false
    )

    if (!(Test-Path -Path $Destination)) {
        Write-Message "Creating missing unzip directory"
        try {
            New-Item -ItemType directory -Path $Destination | Out-Null
            Write-Host "Success" -ForegroundColor Green
        } catch {
            Write-Host "Failed" -ForegroundColor Red
            Write-Host "*** Error message: $_" -ForegroundColor Red
            exit 1
        }
    }
    if ($PSVersionTable.PSVersion.Major -ge 5) {
        # PowerShell 5 introduced Expand-Archive
        Write-Message "Using Expand-Archive to unzip"
        try{
            Expand-Archive -Path $ZipFile -DestinationPath $Destination -Force
            Write-Host "Success" -ForegroundColor Green
        } catch {
            Write-Host "Failed" -ForegroundColor Red
            Write-Host "*** Error message: $_" -ForegroundColor Red
            exit 1
        }
    } else {
        # This method will work with older versions of powershell, but it is
        # slow
        Write-Message "Using Shell.Application to unzip"
        try {
            $objShell = New-Object -Com Shell.Application
            $objZip = $objShell.NameSpace($ZipFile)
            try{
                foreach ($item in $objZip.Items()) {
                    $objShell.Namespace($Destination).CopyHere($item, 0x14)
                }
            } catch {
                Write-Host "Failed" -ForegroundColor Red
                Write-Host "*** Failed to unzip $ZipFile"
                Write-Host "*** Error message: $_" -ForegroundColor Red
                exit 1
            }
            Write-Host "Success" -ForegroundColor Green
        } catch {
            Write-Host "Failed" -ForegroundColor Red
            Write-Host "*** Error message: $_" -ForegroundColor Red
            exit 1
        }
    }
    if ($Clean) {
        Write-Message "Removing zip file"
        Remove-Item $ZipFile -ErrorAction SilentlyContinue
        if (Test-Path -Path $ZipFile) {
            Write-Host "Failed" -ForegroundColor Red
        } else {
            Write-Host "Success" -ForegroundColor Green
        }
    }
}

# Starting with Windows 10 build 1809 and Windows Server 2019 Microsoft included
# 10.0.17763
# OpenSSH as an optional feature
$WIN_VER = (Get-WMIObject Win32_OperatingSystem).version
if ([version]$WIN_VER -ge [version]"10.0.17763") {
    # Install using the Built-in Optional Feature
    Write-Message "Checking OpenSSH feature availability"
    try {
        $server = Get-WindowsCapability -Online | Where-Object Name -like 'OpenSSH.Server*'
        if ($server.Count -gt 0) {
            Write-Host "Success" -ForegroundColor Green
        } else {
            Write-Host "Failed" -ForegroundColor Red
            Write-Host "*** Package not found" -ForegroundColor Red
            exit 1
        }
    } catch {
        Write-Host "Failed" -ForegroundColor Red
        Write-Host "*** Error message: $_" -ForegroundColor Red
        exit 1
    }

    if ($server.State -ne "Installed") {
        Write-Message "Installing OpenSSH feature"
        try {
            $status = Add-WindowsCapability -Online -Name $server.Name
            Write-Host "Success" -ForegroundColor Green
            if ($status.RestartNeeded) {
                Write-Host "*** Restart needed" -ForegroundColor Yellow
            }
        } catch {
            Write-Host "Failed" -ForegroundColor Red
            Write-Host "*** Error message: $_" -ForegroundColor Red
            exit 1
        }
    } else {
        Write-Message "OpenSSH feature already installed"
        Write-Host "Success" -ForegroundColor Green
    }
} else {
    # Install using PowerShell OpenSSH Project binaries

    # Download OpenSSH
    Get-WebFile -Url $OPENSSH_URL -OutFile $OPENSSH_ZIP

    # Unzip OpenSSH
    Expand-ZipFile -ZipFile $OPENSSH_ZIP -Destination $INSTALL_DIR -Clean

    # Move Unzipped Files into Program Files
    Write-Message "Copying files to Program Files"
    try {
        Copy-Item -Path "$INSTALL_DIR\OpenSSH-Win64\*" `
      -Destination "$INSTALL_DIR\" `
      -Recurse `
      -Force
        Write-Host "Success" -ForegroundColor Green
    } catch {
        Write-Host "Failed" -ForegroundColor Red
        Write-Host "*** Error message: $_" -ForegroundColor Red
        exit 1
    }

    # Delete empty directory
    Write-Message "Deleting empty directory"
    try {
        Remove-Item -Path "$INSTALL_DIR\OpenSSH-Win64" -Recurse -Force
        Write-Host "Success" -ForegroundColor Green
    } catch {
        Write-Host "Failed" -ForegroundColor Red
        Write-Host "*** Error message: $_" -ForegroundColor Red
        exit 1
    }

    # Run the install script
    Write-Message "Running OpenSSH Install Script"
    try {
        # I can't suppress output here in Windows 8/2012
        & "$INSTALL_DIR\install-sshd.ps1" *> $null
        Write-Host "Success" -ForegroundColor Green
    } catch {
        Write-Host "Failed" -ForegroundColor Red
        Write-Host "*** Error message: $_" -ForegroundColor Red
        exit 1
    }

    # Configure SSHD
    Write-Message "Starting the SSHD Service to populate config"
    try {
        Start-Service sshd *> $null
        Write-Host "Success" -ForegroundColor Green
    } catch {
        Write-Host "Failed" -ForegroundColor Red
        Write-Host "*** Error message: $_" -ForegroundColor Red
        exit 1
    }

    # Give it time to populate directory
    Write-Message "Waiting 5 seconds for config to populate"
    Start-Sleep -Seconds 5
    Write-Host "Success" -ForegroundColor Green

    Write-Message "Stopping the SSHD Service to modify config"
    try {
        Stop-Service sshd *> $null
        Write-Host "Success" -ForegroundColor Green
    } catch {
        Write-Host "Failed" -ForegroundColor Red
        Write-Host "*** Error message: $_" -ForegroundColor Red
        exit 1
    }
    $SSHD_CONFIG = "$DATA_DIR\sshd_config"

    # Ensure access control on administrators_authorized_keys meets the requirements
    Import-Module "$INSTALL_DIR\OpenSSHUtils.psd1" -Force

    Write-Message "Ensuring config permissions are correct"
    try {
        # I can't suppress output here in Windows 8/2012
        Repair-SshdConfigPermission -FilePath $SSHD_CONFIG -Confirm:$false *> $null
        Write-Host "Success" -ForegroundColor Green
    } catch {
        Write-Host "Failed" -ForegroundColor Red
        Write-Host "*** Error message: $_" -ForegroundColor Red
        exit 1
    }

    Write-Message "Ensuring host key permissions are correct"
    try {
        $ssh_host_keys = Get-ChildItem $DATA_DIR\ssh_host_*_key `
                        -ErrorAction SilentlyContinue
        $ssh_host_keys | ForEach-Object {
            # I can't suppress output here in Windows 8/2012
            Repair-SshdHostKeyPermission -FilePath $_.FullName -Confirm:$false *> $null
        }
        Write-Host "Success" -ForegroundColor Green
    } catch {
        Write-Host "Failed" -ForegroundColor Red
        Write-Host "*** Error message: $_" -ForegroundColor Red
        exit 1
    }
}

# Ensure SSH automatically starts on boot
Write-Message "Setting SSH Service to Auto Start"
try {
    Set-Service sshd -StartupType Automatic
    Write-Host "Success" -ForegroundColor Green
} catch {
    Write-Host "Failed" -ForegroundColor Red
    Write-Host "*** Error message: $_" -ForegroundColor Red
    exit 1
}

# Confirm the Firewall rule exists and is configured
# It should be created automatically by setup if installed via Feature
# Otherwise we need to create it
# Load the rule
$ssh_firewall_rule = Get-NetFirewallRule -Name "OpenSSH-Server-In-TCP" -ErrorAction SilentlyContinue

# If not defined, then create the rule
if (!$ssh_firewall_rule) {
    Write-Message "Creating SSH Firewall rule"
    try {
        New-NetFirewallRule -Name 'OpenSSH-Server-In-TCP' `
            -DisplayName 'OpenSSH Server (sshd)' `
            -Group "OpenSSH Server" `
            -Description "Allow access via TCP port 22 to the OpenSSH Daemon" `
            -Enabled True `
            -Direction Inbound `
            -Protocol TCP `
            -LocalPort 22 `
            -Action Allow | Out-Null
            # TODO: I don't know if we need to add this
            # -Program "$INSTALL_DIR\sshd.exe" `
            # -Program "$($env:WINDIR)\System32\OpenSSH\sshd.exe" `
        Write-Host "Success" -ForegroundColor Green
    } catch {
        Write-Host "Failed" -ForegroundColor Red
        Write-Host "*** Error message: $_" -ForegroundColor Red
        exit 1
    }
} else {
    if (!$ssh_firewall_rule.Enabled) {
        Write-Message "Enabling SSH Firewall Rule"
        try {
            Enable-NetFirewallRule -Name 'OpenSSH-Server-In-TCP' | Out-Null
            Write-Host "Success" -ForegroundColor Green
        } catch {
            Write-Host "Failed" -ForegroundColor Red
            Write-Host "*** Error message: $_" -ForegroundColor Red
            exit 1
        }
    }
}

# Make Powershell the default shell for SSH
Write-Message "Making Powershell the default shell"
try {
    New-Item -Path HKLM:\SOFTWARE\OpenSSH -Force | Out-Null
    New-ItemProperty -Path HKLM:\SOFTWARE\OpenSSH `
        -Name DefaultShell `
        -Value "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" | Out-Null
    Write-Host "Success" -ForegroundColor Green
} catch {
    Write-Host "Failed" -ForegroundColor Red
    Write-Host "*** Error message: $_" -ForegroundColor Red
    exit 1
}

# Start SSHD
Write-Message "Starting the SSHD Service"
try {
    Start-Service sshd *> $null
    Write-Host "Success" -ForegroundColor Green
} catch {
    Write-Host "Failed" -ForegroundColor Red
    Write-Host "*** Error message: $_" -ForegroundColor Red
    exit 1
}

Write-Host "********************************************************************************"
Write-Host "OpenSSH Installed Successfully"
Write-Host "********************************************************************************"
